def table_name = &1 
def number_of_blocks = &2
drop table &table_name;
create table &table_name ( f1 varchar2(10) );
insert into &table_name f1 values ('aaaaaaaaaa');
alter table &table_name minimize records_per_block;
declare
rows number := &number_of_blocks * 2;
begin
-- please mind blocks are added to the segment per extent, not per single block!
for counter in 1..rows loop
  insert into &table_name values ('aaaaaaaaaa');
end loop;    
end;
/
exec dbms_stats.gather_table_stats('','&table_name');
select blocks from user_segments where segment_type = 'TABLE' and segment_name = upper('&table_name');
